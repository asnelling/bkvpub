#  Copyright (C) 2023 BKV. All Rights Reserved.
#
#  Unauthorized copying of this file, via any medium is strictly prohibited.
#  Proprietary and confidential.
#
#  Written and provided as is by Addison Snelling <addison@asnell.io> on March
#  21, 2023, without warranty of any kind, express or implied.
from argparse import Namespace
from http import HTTPStatus
from http.client import HTTPResponse
from socket import socketpair
from typing import Any, Dict
from unittest import IsolatedAsyncioTestCase
from urllib.request import Request

import os

os.environ["BKVCSV_DEBUG"] = "1"

from bkvcsv import SalesforceClient, BkvCsvClient, SftpClient


class BlankNamespace(Namespace):
    csv_host = "http://example.com"

    def __getattr__(self, name: str) -> Any:
        try:
            return super().__getattr__(name)
        except AttributeError:
            return ""


class TestBkvCsvClient(BkvCsvClient):
    class salesforce_class(SalesforceClient):
        postSuccessCalled = False
        response: HTTPResponse

        def _open(self, request: Request):
            return self.response

        def _maybe_auth(self):
            pass

        def getLogId(self):
            return "test_log_id-0000"

        def postSuccess(self, log_id: str) -> bool:
            self.postSuccessCalled = True
            return True

        def getPrefix(self, log_id: str):
            return "test-"

        def setResponse(self, status: HTTPStatus = HTTPStatus.OK,
                        headers: Dict[str, str] = None, payload: str = None):
            if headers is None:
                headers = {}

            if payload is None:
                payload = ""

            sock1, sock2 = socketpair()

            with sock1.makefile("w") as fd:
                fd.write(
                    f"HTTP/1.1 {status.value} {status.phrase}\r\n"
                    "Date: Tue, 16 May 2023 13:07:49 GMT\r\n"
                    f"Content-Length: {len(payload)}\r\n")

                for (key, val) in headers.items():
                    fd.write(f"{key}: {val}\r\n")

                fd.write("\r\n")
                fd.write(payload)

            self.response = HTTPResponse(sock2)
            self.response.begin()
            sock1.close()
            sock2.close()

    class sftp_class(SftpClient):
        putFileCalled = False

        async def putFile(self, inbuf, dstfile: str):
            self.putFileCalled = True

        async def getFilesMatching(self, in_dir: str, prefix: str):
            return []


class TestBkv(IsolatedAsyncioTestCase):
    async def test_cli_import(self):
        payload = (
            "col1,col2,col3\n"
            "red,15,foo one foo two\n"
            "green,30,foo three foo four\n"
        )

        client = TestBkvCsvClient.with_config(BlankNamespace())

        client.force.setResponse(HTTPStatus.OK, headers={
            "Content-Type": "text/csv",
            "Content-Disposition": "attachment; filename=\"test.csv\"",
        }, payload=payload)

        await client.cli_import()
        self.assertEqual(True, client.sftp.putFileCalled)
        self.assertEqual(True, client.force.postSuccessCalled)

    async def test_cli_import_skip_sftp_when_no_rows(self):
        client = TestBkvCsvClient.with_config(BlankNamespace())

        client.force.setResponse(HTTPStatus.NO_CONTENT)

        await client.cli_import()
        self.assertEqual(False, client.sftp.putFileCalled)
        self.assertEqual(True, client.force.postSuccessCalled)

    async def test_cli_export_reports_success_if_nothing_to_do(self):
        client = TestBkvCsvClient.with_config(BlankNamespace())
        await client.cli_export()
        self.assertEqual(True, client.force.postSuccessCalled)
