# bkvcsv

[**Development Progress**](./DEVELOPMENT.md)

`bkvcsv` is a command line application that synchronizes data between Salesforce
tenants and off-platform services. A vendor service in client's Salesforce
tenant implements a read/write CSV interface to Salesforce Objects over HTTP.
An SFTP server provides a symmetrical two-way interface publish and retrieve
updates across various off-platform services. `bkvcsv` is the bridge, passing
updates in both directions between Salesforce and the SFTP server.

`bkvcsv` is designed to run periodically and process several n-megabyte files in
roughly 15 minute intervals as a [Python shell job in AWS Glue][python-glue].
`bkvcsv` supports failure recovery by tagging payloads and reporting their final
disposition to Salesforce. It requires only a Python 3.7 (or newer)
interpreter, and the only dependency is a reasonably recent version of the
`curl` command.

[python-glue]: https://docs.aws.amazon.com/glue/latest/dg/add-job-python.html

`bkvcsv` has two operations named from the perspective of the SFTP server and
consistent with directory names.

- **import** CSV files from Salesforce into the SFTP server, and
- **export** CSV files to Salesforce from the SFTP server 

```
*=================*                            *=================*
|                 |           import           |                 |
|                 |         >>>>>>>>>>         |                 |
|   Salesforce    |                            |   SFTP Server   |
|                 |           export           |                 |
|                 |         <<<<<<<<<<         |                 |
*=================*                            *=================*
```

`bkvcsv` &copy; 2023 BKV. All rights Reserved. See [LICENSE](./LICENSE).

## Installation

Copy `bkvcsv.py` to a directory in `$PATH`; make it executable. For example:

```Shell
# site-wide installation
$ sudo cp bkvcsv.py /usr/local/bin/bkvcsv
$ sudo chmod +x /usr/local/bin/bkvcsv

# install for current user only
$ cp bkvcsv.py ~/.local/bin/bkvcsv
$ chmod +x ~/.local/bin/bkvcsv
```

Verify the installation:

```Shell
$ bkvcsv --help
usage: bkvcsv [-h] ...
```

## Usage

Syntax:

```Shell
$ bkvcsv {--file-type|-T} <file-type> {--command|-K} <operation>
```

Import from Salesforce into FTP Server:

```Shell
$ bkvcsv -T NewOwner -K import
```

Export from FTP server to Salesforce:

```Shell
$ bkvcsv -T PaymentHistory -K export
```

For detailed usage documentation:

```Shell
$ bkvcsv --help
```

## Security

`bkvcsv` will only connect to known FTP servers listed in `~/.ssh/known_hosts`,
the same mechanism used for SSH connections. Before the first run, and each time
the remote host key's change, the user must populate `~/.ssh/known_hosts`.
Consult [VERIFYING HOST KEYS][ssh_verify] in [ssh(1)][man_ssh] for more
information.

To retrieve keys from some remote `host`:

```Shell
$ ssh-keyscan -t rsa <host> | tee -a ~/.ssh/known_hosts
```

Alternatively, you can pass command arguments directly to curl:

```Shell
# manually specify the host key
$ bkvcsv --curl-opts='--hostpubmd5 ad0948c2780ca4d887f3d7633fc70bb5' ...

# query a host for its key in `hostpubmd5` format
$ ssh-keyscan -t rsa sftp.example.com | cut -d' ' -f3 | base64 -d | md5sum
# sftp.example.com:22 SSH-2.0-AWS_SFTP_1.1
ad0948c2780ca4d887f3d7633fc70bb5  -

# turn off host key checking entirely
$ bkvcsv --curl-opts='--insecure' ...
```

**CAUTION:** The above "trust on first use" approach is **not secure** for
exchanging keys on public hosts. Production systems must provision host keys
out-of-band, which is beyond the scope of this documentation.

[ssh_verify]: https://manpages.ubuntu.com/manpages/jammy/man1/ssh.1.html#verifying%20host%20keys

[man_ssh]: https://manpages.ubuntu.com/manpages/jammy/man1/ssh.1.html

## Configuration

`bkvcsv` exposes its configuration through the following command line arguments
and environment variables. Arguments supplied on the command line override their
corresponding environment variable.

The deployed application will be pre-configured with default values for
non-secret parameters. Client must supply, at a minimum, parameters marked
with an asterisk (*).

| CLI Argument              | Environment Variable     | Description                                                                                       | Default                                 |
|---------------------------|--------------------------|---------------------------------------------------------------------------------------------------|-----------------------------------------|
| `-i`, `--client-id`       | `BKVCSV_CLIENT_ID`       | * CSV Service: OAuth2 Client ID                                                                   |                                         |
| `-s`, `--client-secret`   | `BKVCSV_CLIENT_SECRET`   | * CSV Service: OAuth2 Client Secret                                                               |                                         |
| `-u`, `--sftp-username`   | `BKVCSV_SFTP_USERNAME`   | * SFTP Server: Username                                                                           |                                         |
| `-p`, `--sftp-password`   | `BKVCSV_SFTP_PASSWORD`   | * SFTP Server: Password                                                                           |                                         |
| `-T`, `--file-type`       | `BKVCSV_FILE_TYPE`       | * Salesforce: file type                                                                           |                                         |
| `-K`, `--command`         | `BKVCSV_COMMAND`         | * import: transfer from Salesforce to FTP server. export: transfer from FTP server to Salesforce. |                                         |
| `-g`, `--grant-type`      | `BKVCSV_GRANT_TYPE`      | Salesforce: OAuth2 token grant type                                                               | `client_credentials`                    |
| `-H`, `--csv-host`        | `BKVCSV_CSV_HOST`        | Salesforce: Hostname                                                                              | `https://my-salesforce-org.example.com` |
| `-a`, `--user-agent`      | `BKVCSV_USER_AGENT`      | Salesforce: User-agent HTTP header                                                                | `bkvcsv/0.0.0-dev1`                     |
| `-t`, `--token-uri`       | `BKVCSV_TOKEN_URI`       | Salesforce: Oauth2 token endpoint                                                                 | `/services/oauth2/token`                |
| `-c`, `--csv-uri`         | `BKVCSV_CSV_URI`         | Salesforce: Get/Post CSV endpoint                                                                 | `/services/apexrest/CsvWebService`      |
| `-l`, `--log-uri`         | `BKVCSV_LOG_URI`         | Salesforce: Log ID endpoint                                                                       | `/services/apexrest/ProcessLog`         |
| `-f`, `--sftp-conn`       | `BKVCSV_SFTP_CONN`       | SFTP Server: Connection string (scheme + host + directory)                                        | `sftp://sftp.example.com`               |
| `-I`, `--sftp-indir`      | `BKVCSV_SFTP_INDIR`      | SFTP Server: import destination directory (Salesforce -> sftp)                                    | `/salesforce/Import`                    |
| `-O`, `--sftp-outdir`     | `BKVCSV_SFTP_OUTDIR`     | SFTP Server: export source directory (sftp -> Salesforce)                                         | `/salesforce/Export`                    |
| `-A`, `--sftp-archivedir` | `BKVCSV_SFTP_ARCHIVEDIR` | SFTP Server: Export archive directory (sftp -> Salesforce)                                        | `/salesforce/Export/Archive/`           |
| `-C`, `--curl-opts`       | `BKVCSV_CURL_OPTS`       | Space-separated command line options to pass to curl                                              |                                         |
| (none)                    | `BKVCSV_DEBUG`           | Set to 1 to enable debug logging                                                                  |                                         |

**Example: command line arguments**

```Shell
$ bkvcsv -i "Wt3h5iEy..." -s "153498AA..." -u "serendipitty" -p "password123" -T NewOwner -K import
```

**Example: environment variables**

```Shell
export BKVCSV_CLIENT_ID=Wt3h5iEy...
export BKVCSV_CLIENT_SECRET=153498AA...
export SFTP_USERNAME=serendipitty
export SFTP_PASSWORD=password123

$ bkvcsv -T NewOwner -K import
```

## Development

### Running the tests

```Shell
$ python -m unittest discover
```

### Contact

Addison Snelling

- Email: [addison@asnell.io](mailto:addison@asnell.io)
- LinkedIn: [in/addisonsnelling](https://www.linkedin.com/in/addisonsnelling/)
- GitHub: [asnelling](https://github.com/asnelling)

## Copyright

`bkvcsv` &copy; 2023 BKV. All rights Reserved.
