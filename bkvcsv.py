#!/usr/bin/env python3
import asyncio
import json
import logging
import os
import shutil
from argparse import ArgumentParser, Namespace
from functools import lru_cache
from http.client import HTTPResponse
from pathlib import Path
from typing import Mapping, List, Dict, Union
from urllib import request
from urllib.parse import urlencode
from urllib.request import Request

#  Copyright (C) 2023 BKV. All Rights Reserved.
#
#  Unauthorized copying of this file, via any medium is strictly prohibited.
#  Proprietary and confidential.
#
#  Written and provided as is by Addison Snelling <addison@asnell.io> on March
#  21, 2023, without warranty of any kind, express or implied.

# logging.basicConfig(level=logging.DEBUG)
if os.getenv("BKVCSV_DEBUG", "False") == "1":
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)

BKVCSV_USE_OAUTH2_INSTANCE_URL = True
BKVCSV_ENVVAR_PREFIX = "BKVCSV_"
BKVCSV_LOGID_KEY = "logId"
BKVCSV_FILETYPE_KEY = "fileType"


def command_parser():
    parser = ArgumentParser(
        description="Import and export files to and from FTP and Salesforce."
    )

    # The following options must be provided on the command line or in environment variables:
    parser.add_argument(
        "-K",
        "--command",
        help="import: transfer from Salesforce to FTP server. export: transfer from FTP server to Salesforce.",
        choices=["import", "export"],
        required=True,
    )
    parser.add_argument("-i", "--client-id", help="* Salesforce: OAuth2 Client ID")
    parser.add_argument(
        "-s", "--client-secret", help="* Salesforce: OAuth2 Client Secret"
    )
    parser.add_argument("-u", "--sftp-username", help="* SFTP Server: Username")
    parser.add_argument("-p", "--sftp-password", help="* SFTP Server: Password")
    parser.add_argument(
        "-T", "--file-type", help="* Salesforce: file type", required=True
    )

    # The rest have sane defaults matching the target environment.
    parser.add_argument(
        "-g",
        "--grant-type",
        help="Salesforce: OAuth2 token grant type",
        default="client_credentials",
    )
    parser.add_argument(
        "-H",
        "--csv-host",
        help="Salesforce: Hostname",
        default="https://my-salesforce-org.example.com",
    )
    parser.add_argument(
        "-a",
        "--user-agent",
        help="Pass this user agent header to remote peers",
        default="bkvcsv/0.0.0-dev1",
    )
    parser.add_argument(
        "-t",
        "--token-uri",
        help="Salesforce: Oauth2 token endpoint",
        default="/services/oauth2/token",
    )
    parser.add_argument(
        "-c",
        "--csv-uri",
        help="Salesforce: Get/Post CSV endpoint",
        default="/services/apexrest/CsvWebService",
    )
    parser.add_argument(
        "-l",
        "--log-uri",
        help="Salesforce: Log ID endpoint",
        default="/services/apexrest/ProcessLog",
    )
    parser.add_argument(
        "-f",
        "--sftp-conn",
        help="SFTP Server: Connection string (scheme + host + directory)",
        default="sftp://sftp.example.com",
    )
    parser.add_argument(
        "-I",
        "--sftp-indir",
        help="SFTP Server: import destination directory (Salesforce -> sftp)",
        default="/salesforce/Import/",
    )
    parser.add_argument(
        "-O",
        "--sftp-outdir",
        help="SFTP Server: export source directory (sftp -> Salesforce)",
        default="/salesforce/Export/",
    )
    parser.add_argument(
        "-A",
        "--sftp-archivedir",
        help="SFTP Server: Export archive directory (sftp -> Salesforce)",
        default="/salesforce/Export/Archive/",
        type=Path,
    )
    parser.add_argument(
        "-C",
        "--curl-opts",
        help="Space-separated command line options to pass to curl",
        default="",
    )

    return parser


MIME_JSON = "application/json"
MIME_CSV = "text/csv"

KEY_Accept = "Accept"
KEY_Authorization = "Authorization"
KEY_UserAgent = "User-Agent"
KEY_ContentType = "Content-Type"

CURL_CMD = "curl"
CURL_FTP_METHOD_NOCWD = "nocwd"
CURL_OPT_FTP_METHOD = "--ftp-method"
CURL_OPT_LIST = "--list-only"
CURL_OPT_OUTPUT = "--output"
CURL_OPT_QUOTE = "--quote"
CURL_OPT_UPLOAD_FILE = "--upload-file"
CURL_OPT_USER = "--user"
CURL_QUOTE_RENAME = "rename"
CURL_FILENAME_STDIO = "-"


class BkvCsvError(Exception):
    pass


class Client:
    ns: Namespace

    @classmethod
    def with_config(cls, ns: Namespace):
        client = cls()
        setattr(client, "ns", ns)
        return client


class SalesforceClient(Client):
    def _authenticate(self):
        url = "%s%s" % (self.ns.csv_host, self.ns.token_uri)
        req_body = {
            "grant_type": self.ns.grant_type,
            "client_id": self.ns.client_id,
            "client_secret": self.ns.client_secret,
        }
        logging.info("salesforce: requesting auth token, grant_type: %s", self.ns.grant_type)
        logging.debug("salesforce:     client_id: %s, client_secret: %s", self.ns.client_id, self.ns.client_secret)

        req = Request(url, data=urlencode(req_body).encode(), headers=self.headers)
        res = self._open(req)
        require_success(res)
        res_body = json.load(res)

        self.headers[KEY_Authorization] = "%(token_type)s %(access_token)s" % res_body
        logging.info("salesforce: received auth token, type: %s", res_body["token_type"])
        logging.debug("salesforce:     %s", res_body["access_token"])

        instance_url = res_body.get("instance_url")
        if instance_url and BKVCSV_USE_OAUTH2_INSTANCE_URL:
            print(
                "Overriding `BKVCSV_CSV_HOST` with `instance_url` from OAuth2 "
                "server. Disable this by setting"
                "`BKVCSV_USE_OAUTH2_INSTANCE_URL = False`."
            )
            self.ns.csv_host = instance_url

        return None

    def getLogId(self):
        query_params = {
            BKVCSV_FILETYPE_KEY: self.ns.file_type,
        }

        res = self._request(self.ns.log_uri, query_params=query_params)
        res_body = json.load(res)
        log_id = res_body.get(BKVCSV_LOGID_KEY)
        logging.info("salesforce: get log ID: %s", log_id)

        return log_id

    def _getCsv(
        self,
        log_id: str,
        headers: Dict[str, str] = None,
    ):
        query_params = {
            BKVCSV_LOGID_KEY: log_id,
            BKVCSV_FILETYPE_KEY: self.ns.file_type,
        }

        res = self._request(self.ns.csv_uri, headers=headers, query_params=query_params)

        return res

    def getCsv(
        self,
        log_id: str,
    ):
        headers = {
            KEY_Accept: MIME_CSV,
        }

        res = self._getCsv(log_id, headers=headers)
        filename = res.headers.get_filename()

        logging.info("salesforce: get CSV (log id: %s) (%d bytes): %s", log_id, res.length, filename)

        return filename, res

    def getPrefix(
        self,
        log_id: str,
    ) -> str:
        res = self._getCsv(log_id)
        res_body = json.load(res)
        prefix = res_body.get("fileNamePrefix")
        logging.info("salesforce: get file prefix (log id: %s): %s", log_id, prefix)

        return prefix

    def postCsv(self, log_id: str, data: bytes) -> bool:
        logging.info("salesforce: upload CSV (%d bytes): %s", len(data), log_id)

        query_params = {
            BKVCSV_LOGID_KEY: log_id,
            BKVCSV_FILETYPE_KEY: self.ns.file_type,
        }
        headers = {
            KEY_ContentType: MIME_CSV,
        }

        self._request(
            self.ns.csv_uri, headers=headers, data=data, query_params=query_params
        )

        return True

    def postSuccess(self, log_id: str) -> bool:
        logging.info("salesforce: log success: %s", log_id)

        query_params = {
            BKVCSV_LOGID_KEY: log_id,
        }

        self._request(self.ns.log_uri, method="POST", query_params=query_params)

        return True

    def _maybe_auth(self):
        if KEY_Authorization not in self.headers:
            logging.info("Unauthenticated. Need to request OAuth token.")
            self._authenticate()

    def _request(
        self,
        path: str,
        headers: Dict[str, str] = None,
        query_params: Dict[str, str] = None,
        **kwargs,
    ) -> HTTPResponse:
        url = self.ns.csv_host + path

        self._maybe_auth()

        final_headers = {
            **self.headers,
        }

        if headers is not None:
            final_headers.update(headers)

        if query_params is not None:
            url += "?" + urlencode(query_params)

        req = Request(url, headers=final_headers, **kwargs)

        logging.info("salesforce: HTTP > (%d bytes) %s %s", len(req.data or ""), req.get_method(), req.get_full_url())
        response = self._open(req)
        response_size = response.headers.get("content-length", "?")
        logging.info("salesforce: HTTP < (%s bytes) %d %s", response_size, response.status, response.headers.get_content_type())
        require_success(response)

        return response

    def _open(self, req: Request):
        return request.urlopen(req)

    @property
    @lru_cache(maxsize=None)
    def headers(self):
        headers = {
            KEY_Accept: MIME_JSON,
            KEY_UserAgent: self.ns.user_agent,
        }
        return headers


class SftpClient(Client):
    async def putFile(self, inbuf, dstfile: str):
        logging.info("sftp: put %s", dstfile)

        return await self._curl(
            CURL_OPT_UPLOAD_FILE, CURL_FILENAME_STDIO, path=dstfile, postdata=inbuf
        )

    async def getFile(self, srcfile: Union[str, Path]):
        logging.info("sftp: get %s", srcfile)

        stdout, stderr = await self._curl(
            CURL_OPT_OUTPUT, CURL_FILENAME_STDIO, path=srcfile
        )
        return stdout

    async def list(self, parent: str):
        logging.info("sftp: ls %s", parent)
        if parent[-1] != "/":
            parent += "/"
        stdout, stderr = await self._curl(CURL_OPT_LIST, path=parent)
        return stdout.decode().splitlines()

    async def getFilesMatching(self, in_dir: str, prefix: str) -> List[str]:
        """
        List the immediate children of `dir` the start with `prefix`.
        For example, suppose FTP server has the following hierarchy:


           * home

               * bob

               * alice

                   * shared_files

                       * bar_a

                       * bar_b

                       * foo_a

                       * foo_b

                   * private

                       * foo_a

                       * foo_b

        Calling :func:`getFilesMatching` with ``dir``
        ``/home/alice/shared_files/`` and ``prefix`` ``foo`` will
        return an array with two elements:

        .. code-block:: matching-files

            [ Path('/home/alice/shared_files/foo_a'),
              Path('/home/alice/shared_files/foo_b'), ]

        :param in_dir: match against children in `dir`.
        :param prefix: filename prefix filter.
        :return: absolute paths to files under `dir` starting with `prefix`.
        """
        remote_files = await self.list(in_dir)
        out = [x for x in remote_files if x[: len(prefix)] == prefix]
        out = [str(Path(in_dir) / x) for x in out]

        return out

    async def rename(self, src: str, dst: str):
        logging.info("sftp: rename %s %s", src, dst)

        destdir, destfile = dst.rsplit("/", maxsplit=1)
        destdir += "/"

        stdout, stderr = await self._curl(
            CURL_OPT_QUOTE,
            f'{CURL_QUOTE_RENAME} "{src}" "{dst}"',
            path=destdir,
        )
        if destfile not in stdout.decode():
            raise BkvCsvError(f"{destfile} not found in {destdir} after rename.")

    async def _curl(self, *args: str, path: str = "", postdata: HTTPResponse = None):
        """
        Launch `curl` in a separate process and wait for completion.

        :param path: append to base URL
        :param args: command line arguments passed to `curl`.
        :param postdata: request payload, must support `read()`.
        :return:
        """
        curl_args = [
            *self.ns.curl_opts.split(" "),
            CURL_OPT_FTP_METHOD,
            CURL_FTP_METHOD_NOCWD,
            CURL_OPT_USER,
            f"{self.ns.sftp_username}:{self.ns.sftp_password}",
            *args,
            self.ns.sftp_conn + path,
        ]
        logging.debug("sftp: calling curl with args: %s", " ".join(curl_args))

        process = await asyncio.create_subprocess_exec(
            shutil.which(CURL_CMD),
            *curl_args,
            stderr=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE,
            stdin=asyncio.subprocess.PIPE,
        )

        if postdata is not None:
            while True:
                line = postdata.readline()
                if not line:
                    break
                process.stdin.write(line)
            await process.stdin.drain()
            process.stdin.close()

        stdout_data, stderr_data = await process.communicate()

        if process.returncode != 0:
            raise BkvCsvError(
                f"curl exited with return code: {process.returncode}: {stderr_data.decode()}"
            )

        return stdout_data, stderr_data


class BkvCsvClient(Client):
    """
    Entrypoint for the CLI.

    From the CLI: methods under this class beginning with `cli_` are
    callable by passing a name as the positional argument to the CLI (called
    `command` in the usage description).

    "sub clients" for interacting with an FTP server and Salesforce are
    available under `client.sftp` and `client.force`, respectively, if your
    `BkvCsvClient` instance is called `client`.
    """

    salesforce_class = SalesforceClient
    sftp_class = SftpClient

    async def cli_import(self):
        log_id = self.force.getLogId()
        dst_filename, csv = self.force.getCsv(log_id)

        if csv.status == 200:
            logging.info(f"import: uploading CSV %s (log id: %s)", dst_filename, log_id)
            await self.sftp.putFile(csv, self.ns.sftp_indir + dst_filename)
        else:
            logging.info(f"import: skip empty CSV (log id: %s)", log_id)

        self.force.postSuccess(log_id)

    async def cli_export(self):
        # get an initial log_id to call `getPrefix`
        log_id = self.force.getLogId()
        file_prefix = self.force.getPrefix(log_id)

        files = await self.sftp.getFilesMatching(self.ns.sftp_outdir, file_prefix)

        if len(files) == 0:
            logging.info("export: nothing to do, no matches for prefix %s", file_prefix)
            self.force.postSuccess(log_id)
            return

        logging.info(f"export: %d files", len(files))

        errors = []
        filename = ""

        for f in files:
            try:
                if log_id is None:
                    log_id = self.force.getLogId()

                payload = await self.sftp.getFile(f)
                self.force.postCsv(log_id, payload)
                log_id = None
                filename = str(Path(f).name)

                # move exported file to archive on FTP server
                archived_path = self.ns.sftp_archivedir / filename
                await self.sftp.rename(f, str(archived_path))
            except Exception as e:
                logging.error(
                    "export: failed for file: %s, log id: % s", filename, log_id, exc_info=e
                )
                errors += [e]

        if errors:
            raise BkvCsvError(f"failed processing {len(errors)} files.")

    @property
    @lru_cache(maxsize=None)
    def force(self):
        return self.salesforce_class.with_config(self.ns)

    @property
    @lru_cache(maxsize=None)
    def sftp(self):
        return self.sftp_class.with_config(self.ns)


def require_success(response: HTTPResponse):
    if response.status not in range(200, 300):
        raise BkvCsvError(
            f"received non-success HTTP status from server: {response.status}"
        )


def env_namespace_factory(environ: Mapping[str, str]):
    """
    Construct a Namespace that recognizes both environment variables and CLI
    configuration.

    :param environ: current environment variables
    :return: a Namespace pre-populated with configuration specified in
        environment variables. `argparse` and other Namespace consumers are
        clueless to this and will clobber configs from the CLI parameters,
        which is the behavior documented in README.
    """

    def env2cli_option(k: str) -> str:
        # out_k = k.removeprefix(BKVCSV_ENVVAR_PREFIX) # Python >=3.9
        out_k = k[len(BKVCSV_ENVVAR_PREFIX) :]  # Python < 3.9

        out_k = out_k.lower()
        return out_k

    kwargs = {
        env2cli_option(k): v
        for (k, v) in environ.items()
        if k.startswith(BKVCSV_ENVVAR_PREFIX)
    }
    ns = Namespace(**kwargs)

    return ns


def main():
    parser = command_parser()

    args, unknown_args = parser.parse_known_args(
        namespace=env_namespace_factory(os.environ),
    )

    if unknown_args:
        logging.debug("unrecognized command arguments: %s", " ".join(unknown_args))

    # Positional argument `command` is the name of a function in BkvCsvClient
    # that implements the given command. Anything it needs is passed through via
    # `args` (type: Namespace), so call it without arguments.
    client = BkvCsvClient.with_config(args)
    command = "cli_" + args.command
    fn = getattr(client, command)
    asyncio.run(fn())


if __name__ == "__main__":
    main()
